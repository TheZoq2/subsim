use std::f32::consts::PI;

use bevy::prelude::*;
use bevy_rapier3d::prelude::*;

fn main() {
    App::new()
        .add_plugins(DefaultPlugins)
        .add_plugins(RapierPhysicsPlugin::<NoUserData>::default())
        .add_plugins(RapierDebugRenderPlugin::default())
        .add_systems(Startup, setup_graphics)
        .add_systems(Startup, setup_physics)
        .add_systems(Update, bouyancy_applyer)
        .add_systems(Update, input_handler)
        .add_systems(Update, bouyancy_scale_updater)
        .add_systems(Update, text_updater)
        .run();
}

fn setup_graphics(mut commands: Commands) {
    // Add a camera so we can see the debug-render.
    commands.spawn(Camera3dBundle {
        transform: Transform::from_xyz(-3.0, 3.0, 8.0).looking_at(Vec3::ZERO, Vec3::Y),
        ..Default::default()
    });

    commands.spawn(PointLightBundle {
        point_light: PointLight {
            intensity: 1500.0,
            shadows_enabled: true,
            ..default()
        },
        transform: Transform::from_xyz(4.0, 8.0, 4.0),
        ..default()
    });
}

fn setup_physics(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
) {
    /* Create the ground. */
    commands
        .spawn(Collider::cuboid(100.0, 0.1, 100.0))
        .insert(PbrBundle {
            mesh: meshes.add(Mesh::from(shape::Plane {
                size: 100.0,
                ..Default::default()
            })),
            material: materials.add(Color::rgb(0.8, 0.7, 0.6).into()),
            transform: Transform::from_xyz(0.0, 0.5, 0.0),
            ..default()
        })
        .insert(TransformBundle::from(Transform::from_xyz(0.0, -2.0, 0.0)));

    let mut transform = Transform::from_xyz(0.0, 2.0, 0.0);
    transform.rotate_x(PI / 2.);

    /* . */
    commands
        .spawn(RigidBody::Dynamic)
        .with_children(|children| {
            let main_radius = 0.11 / 2.;
            let main_height = 0.5;
            let sub_radius = 0.063 / 2.;

            // Main body
            children
                .spawn(Collider::cylinder(main_height, main_radius))
                .insert(PbrBundle {
                    mesh: meshes.add(Mesh::from(shape::Cylinder {
                        radius: main_radius,
                        height: main_height,
                        ..Default::default()
                    })),
                    material: materials.add(Color::rgb(0.8, 0.7, 0.6).into()),
                    ..Default::default()
                });

            let bdevice_diameter = 0.05;
            let bouyancy_device = BouyancyDevice {
                diameter: bdevice_diameter,
                max_displacement: 0.05,
                current_displacement: 0.,
                speed: 0.03,
            };

            let mut add_device = |transform| {
                children
                    .spawn(Collider::cylinder(0.2, sub_radius))
                    .insert(bouyancy_device.clone())
                    .insert(SpatialBundle::from(Transform::from_xyz(0., 0., 0.)))
                    .with_children(|c| {
                        c.spawn(PbrBundle {
                            mesh: meshes.add(Mesh::from(shape::Cylinder {
                                radius: bdevice_diameter,
                                height: 0.1,
                                ..Default::default()
                            })),
                            material: materials.add(Color::rgb(0.8, 0.7, 0.6).into()),
                            ..default()
                        })
                        .insert(TransformBundle::default())
                        .insert(Plunger {});

                        c.spawn(PbrBundle {
                            mesh: meshes.add(Mesh::from(shape::Cylinder {
                                radius: bdevice_diameter,
                                height: 0.1,
                                ..Default::default()
                            })),
                            material: materials.add(Color::rgb(0.8, 0.7, 0.6).into()),
                            ..default()
                        })
                        .insert(TransformBundle::from(Transform::from_xyz(0.0, 0.1, 0.0)));
                    })
                    .insert(transform);
            };

            add_device(TransformBundle::from(Transform::from_xyz(0.10, 0.25, -0.0)));
            add_device(TransformBundle::from(Transform::from_xyz(
                -0.10, 0.25, -0.0,
            )));
            add_device(TransformBundle::from(Transform::from_xyz(
                0.10, -0.25, -0.0,
            )));
            add_device(TransformBundle::from(Transform::from_xyz(
                -0.10, -0.25, -0.0,
            )));
        })
        .insert(ColliderMassProperties::Density(1.))
        .insert(ReadMassProperties::default())
        .insert(Restitution::new(1.0))
        .insert(ExternalForce {
            force: Vec3::new(0., 0., 0.),
            torque: Vec3::new(0., 0., 0.),
        })
        .insert(SpatialBundle::from(transform));

    commands.spawn(TextBundle::from_section("Depth", TextStyle {
        font_size: 15.,
        .. Default::default()
    }))
    .insert(DepthText{});
}

fn bouyancy_applyer(
    mut q_parents: Query<(
        &mut ExternalForce,
        &ReadMassProperties,
        &Children,
        &Transform,
    )>,
    bouyancy_devices: Query<(&BouyancyDevice, &Transform)>,
) {
    for (mut force, body, children, parent_transform) in q_parents.iter_mut() {
        force.torque = Vec3::ZERO;
        force.force = Vec3::ZERO;

        *force += ExternalForce::at_point(
            Vec3::new(0., body.0.mass * 9.81, 0.),
            parent_transform.translation,
            parent_transform.translation,
        );

        for child in children.iter() {
            if let Ok((device, child_transform)) = bouyancy_devices.get(child.to_owned()) {
                println!("{}", device.current_displacement);
                *force += ExternalForce::at_point(
                    Vec3::new(0., device.force(), 0.),
                    parent_transform
                        .mul_transform(child_transform.clone())
                        .translation,
                    parent_transform.translation,
                )
            }
        }

        // *sub += ExternalForce::at_point(
        //     Vec3::new(0., device.force(), 0.),
        //     transform.translation,
        //     Vec3::new(0., 0., 0.),
        // )
    }
}

fn text_updater(
    crafts: Query<(&ExternalForce, &Transform)>,
    mut texts: Query<(&mut Text, &DepthText)>,
) {
    for (_force, transform) in crafts.iter() {
        for (mut text, _marker) in texts.iter_mut() {
            text.sections[0].value = format!("Depth: {:.02}", transform.translation.y)
        }
    }
}

fn bouyancy_scale_updater(
    mut d: Query<(&BouyancyDevice, &mut Children)>,
    mut cylinders: Query<(&mut Transform, &Plunger)>,
) {
    for (device, children) in d.iter_mut() {
        for child in children.iter() {
            if let Ok((mut transform, _plunger)) = cylinders.get_mut(child.to_owned()) {
                transform.translation.y = device.current_displacement
            }
            // cylinders.get_mut(child.to_owned()).unwrap().scale =
            //     Vec3::new(1., 1., 1.) + Vec3::new(1., 1., 1.) * device.current_displacement;
        }
    }
}

fn input_handler(
    mut bouyancy_devices: Query<(&mut BouyancyDevice, &Transform)>,
    input: Res<Input<KeyCode>>,
    time: Res<Time>,
) {
    let input_vector = input
        .pressed(KeyCode::I)
        .then(|| Vec3::new(0., 1., 0.))
        .unwrap_or_default()
        + input
            .pressed(KeyCode::K)
            .then(|| Vec3::new(0., -1., 0.))
            .unwrap_or_default()
        + input
            .pressed(KeyCode::J)
            .then(|| Vec3::new(-1., 0., 0.))
            .unwrap_or_default()
        + input
            .pressed(KeyCode::L)
            .then(|| Vec3::new(1., 0., 0.))
            .unwrap_or_default();

    let global = input.pressed(KeyCode::S).then(|| -1.0).unwrap_or_default()
        + input.pressed(KeyCode::W).then(|| 1.0).unwrap_or_default();

    for (mut device, transform) in bouyancy_devices.iter_mut() {
        let contribution = input_vector.dot(transform.translation.normalize());

        device.current_displacement += (contribution + global) * time.delta_seconds() * 0.05;
    }
}

#[derive(Component, Clone)]
struct BouyancyDevice {
    pub diameter: f32,
    pub max_displacement: f32,
    pub current_displacement: f32,
    pub speed: f32,
}

impl BouyancyDevice {
    fn force(&self) -> f32 {
        PI * (self.diameter / 2.).powf(2.) * self.current_displacement * 9.81
    }
}

#[derive(Component, Clone)]
struct Plunger {}

#[derive(Component, Clone)]
struct DepthText {}
